import React from 'react';

const RegisterForm = props => {


    const onRegisterClicked = ev =>  {
        console.log('EVENT: ', ev.target);
        props.click({
            success: true,
            message: 'Registered successfully'
        });
        
    };

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username.." />
            </div>

            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password.." />
            </div>

            <div>
                <button type="button" onClick={ onRegisterClicked }>Register</button>
            </div>

        </form>
    );
}

export default RegisterForm;